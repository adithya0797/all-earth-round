import datetime
import urllib.request
import math
import json
from scipy import spatial

#some basic skeletal code for classes and accesing the ISS api using json given by Instructor dr Jesse Hartloff


# Location class used to communicate lat/long

class Location:
    # Do not put member variable outside the constructor
    # Variables declared here become static class variables

    # constructor in python. "self" is similar to "thunis" and must be the first parameter in every method declaration
    def __init__(self, latitude, longitude):
        # self variables declared in the constructor become member variables
        self.latitude = latitude
        self.longitude = longitude if longitude < 180.0 else longitude - 360.0

    def getLatitude(self):
        return self.latitude

    def getLongitude(self):
        return self.longitude

    def __str__(self):
        return "(" + str(self.latitude) + "," + str(self.longitude) + ")"


# The primary class that needs to be implemented. Some skeleton code has been provided, but is not necessary. Feel free
# to structure your code any way you want as long as the required methods function properly.
class PaleBlueDot:
    # Do not put member variable outside the constructor
    # Variables declared here become static class variables

    # Parse files and initialize all data structures. The citiesFile is in the format of worldCities.csv
    def __init__(self, citiesFile):
        self.cityLocations = {}
        self.locationCity = {}  # initialize a dictionary. You can use any data structure you choose
        self.cityCoordinate = {}
        self.coordinateCity = {}

        self.codeToName = {}  # initialize a list. You can use any data structure you choose
        self.codeLocation = {}
        self.coordinateCode = {}
        self.codeCoordinate = {}

        self.data = []  # list of coordinate lists, contains all coordinates for cities
        self.obser = [] # observatory coordinates

        self.parseCities(citiesFile)
        self.getObservatories()
        self.tree = spatial.KDTree(self.data)
        self.obsTree = spatial.KDTree(self.obser)

#author: Adithya
    def parseCities(self, citiesFile):
        cities = open(citiesFile, "r")  # open a file in read mode
        cities.readline()  # remove source
        cities.readline()  # remove header
        for line in cities.readlines():
            temp = line.split(",", maxsplit= -1)
            name = temp[2] + ',' + temp[1] + ',' + temp[0]
            latitude = line[3]
            longitude = line[4]
            self.cityLocations[name] = Location(latitude, longitude)
            self.locationCity[self.cityLocations[str]] = name
            self.cityCoordinate[name] = self.makeCoordinates(latitude, longitude)
            self.coordinateCity[self.cityCoordinate[name]] = name
            self.data.append(self.cityCoordinate[name])
            # pass
        cities.close()
#author: Adithya and Jesse
    # pull observatory information from the Internet, parse and store.
    def getObservatories(self):
        url = "http://www.minorplanetcenter.net/iau/lists/ObsCodes.html"
        # Open the url in a browser to see the format of the data.
        result = urllib.request.urlopen(url)  # download the data
        observatories = result.read().decode('utf-8')  # read the data as unicode
        for line in observatories:
            obsCode = line[0:3]
            longitude = line[6:12]
            cos = line[13:20]
            sin = line[21:28]
            Name = line[29:len(line) - 1]

            self.codeToName[obsCode] = Name
            self.codeLocation[obsCode] = Location(self.latitude(longitude, sin, cos), longitude)
            self.coordinateCode[self.makeCoordinates(self.latitude(longitude, sin, cos), longitude)] = obsCode
            self.codeCoordinate[obsCode] = self.makeCoordinates(self.latitude(longitude, sin, cos), longitude)
            self.obser.append(self.codeCoordinate[obsCode])
            # pass
# #author : Adithya

    # part 1 - String Parsing

    '''
    Given the name a city, its region, and country, returns its location as a Location object. Returns an empty list
    if the city is unknown, though bad inputs will not be tested for grading so an error can be thrown instead.
    '''
    def getCityLocation(self, city, region, country):
        name = city + ',' + region + ',' + country
        return self.cityLocations[name]
        # pass

    '''
    Given a 3 digit observatory code as a string, return the name of the corresponding observatory.

    Current data can be found here: http://www.minorplanetcenter.net/iau/lists/ObsCodes.html

    Note that this data is not in the most friendly format and care must be taken while parsing. Non-existing codes
    will not be tested.
    '''

    def getObservatoryName(self, observatoryCode):
        return self.codeToName[observatoryCode]
        # pass
# #author : Adithya

    # part 2 - Math

    '''
    Given a 3 digit observatory code as a string, return the location of the corresponding observatory
    as a Location object with lat /long in degrees. Note that the data is given as
    longitude (in degrees), cos, and sin. Computing atan(sin/cos) will give the latitude in radians.
    Non-existing codes will not be tested.
    '''

    def getObservatoryLocation(self, observatoryCode):
        self.codeCoordinate[observatoryCode]
        # pass

    '''
    Return the great circle distance between two locations on Earth in kilometers. For information on great circle
    distance including sample code in JavaScript see: http://www.movable-type.co.uk/scripts/latlong.html
    '''

    def greatCircleDistance(self, location1, location2):
        r = 6371
        phi1 = math.radians(location1.getLatitude())
        phi2 = math.radians(location2.getLatitude())
        deltaphi = math.radians(location2.getLatitude() - location1.getLatitude())
        deltalambda = math.radians(location2.getLongitude() - location1.getLongitude())
        a = math.sin(deltaphi/2)*math.sin((deltaphi/2)) + math.cos(phi1)*math.cos(phi2)*math.sin(deltalambda/2)*math.sin(deltalambda/2)
        c = 2*math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = r*c
        return d
        # pass

    # part 3

    '''
    Given a location on Earth by lat/long, return the code of the closest observatory in terms of
    great circle distance
    '''
# #author : Adithya

    def getClosestObservatory(self, location):
        coordinates = self.makeCoordinates(location.getLatitude(), location.getLongitude())
        d, i = self.obsTree.query(coordinates, 1)
        return self.coordinateCode[self.obser[i]]
        # pass

    '''
    Return the code of the observatory that is closest to the ISS. ISS location can be obtained through
    this API: http://api.open-notify.org/
    The result will be in json form which python will parse using "json.loads(jsonData)"
    '''
# #author : Adithya

    def getClosestObservatoryToISS(self):
        url = "http://api.open-notify.org/iss-now.json"
        req = urllib.request.urlopen(url)
        data = json.loads(req.read())
        locat = Location(data['iss_position']['latitude'], data['iss_position']['longitude'])
        return self.getClosestObservatory(locat)
        # pass

    '''
    Return the next chance to observe the ISS from the given location. Use the same API from the previous
    method, but call "pass time" to retrieve the relevant data. Parsing the JSON will result in a unix
    timestamp that must be converted and returned in a user friendly format via:
    datetime.datetime.fromtimestamp(int("1457985637")).strftime('%Y-%m-%d %H:%M:%S')
    This is the format expected during testing.
    '''
# author Adithya and Jesse
    def nextPassTime(self, location):
        latitude = location.getLatitude()
        longitude = location.getLongitude()
        url = "http://api.open-notify.org/iss-pass.json?lat=" + latitude + "&lon=" + longitude
        req = urllib.request.urlopen(url)
        data = json.loads(req.read())
        timestamp = data['response'][0]['risetime']
        return datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
        # pass

# #author : Adithya

    # part 4

    '''
    Given a location on Earth by lat/long, return the name of the closest city in terms of
    great circle distance. Credit will only be given if the efficiency bound is met. No
    partial credit for correctness. Return the city as a list in the form [city,region,countryCode]
    '''

    def getClosestCity(self, location):
        coordinates = self.makeCoordinates(location.getLatitude(), location.getLongitude())
        d, i = self.tree.query(coordinates, 1)
        return self.cityCoordinate[self.data[i]].split(',')
        # pass

    '''
    Return the closest city to the ISS. Return city as a list in the form [city,region,countryCode]
    '''

    def getClosestCityToISS(self):
        url = "http://api.open-notify.org/iss-now.json"
        req = urllib.request.urlopen(url)
        data = json.loads(req.read())
        locat = Location(data['iss_position']['latitude'], data['iss_position']['longitude'])
        return self.getClosestCity(locat)
        pass

    def makeCoordinates(self, latitude, longitude):
        r = 6371
        x = r*math.cos(math.radians(latitude))*math.cos(math.radians(longitude))
        y = r*math.cos(math.radians(latitude))*math.sin(math.radians(longitude))
        z = r*math.sin(math.radians(latitude))
        return [x, y, z]
        pass

    def latitude(self, longitude, sin, cos):
        return math.atan2(cos, sin)
        pass

